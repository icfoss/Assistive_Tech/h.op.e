# H.OP.E

**Human Operated Exo Skeleton (H.OP.E)**

A power assisting device, or an exoskeleton is currently one of the important fields in robotics that are widely researched for applications including rehabilitation and physiotherapy, human assistance, power amplification and for other haptic applications.Ex-oskeleton robotis a kind of wearable anthropomorphic power outputmechanism forbody physical enhancement, strength amplication, load carrying, exercise rehabilitationand walking assistance.

Stroke and nerve damage incidence may cause severe upper limb impairment, reducingthe quality of life of patients who are unable to carry out daily functional tasks.Strokeis the second leading cause of death with 5.9 million incidents in 2010 with prevalencebeing more significant in the low and middle-income countries. Patients often have areduced ability to engage in activities of daily living (ADL) due to motor impairment andidentify recovery of their upper limb functions as a rehabilitation priority.Studies haveshown that only 18 out of 100 patients with severe upper limb impairment will attain fullrecovery of their motor functions. Activities of daily living (ADL) require an individ-ual to have a certain level of mobility. Various paretic conditions and muscular disordersmay significantly reduce the ability of patients to conduct ADL and lower their qualityof life.The shoulder is one of the most versatile joints in the human body allowing theupper limb to perform a variety of activities of daily living (ADLs) such as eating, man-aging personal hygiene, and dressing. The versatility of the shoulder is partly due to thewide range of motion of the glenohumeral joint and the coordinated movement of thehumerus and the shoulder girdle, called scapulohumeral rhythm.Repetitive task-basedrehabilitation of the affected limb is often prescribed so that patients can regain all or atleast some of their mobility.

Traditionally, physiotherapists conduct repetitive task-based rehabilitative exercisesto help patients regain functionalmotor skills.But,traditionalrehabilitation throughphysiotherapy is labor intensive and requires one-on-one therapistpatienttreatments,placing additional strain and fatigue on even proficient therapists.Research performedwith robotic training devices,introduced during the last two decades,show that thesedevices are effective in motor rehabilitation and can potentially reduce the expense oftreatment.

Due to the absence of anthropomorphicdesign, range of motion (ROM) of several exoskeletons are limited. So, the **H.OP.E** project collaborated with **College of Engineering Trivandrum(CET)** and **International Centre for Free and Open Source Software (ICFOSS)** mainly concentrated on the above research gaps in the field of exoskeletons. The main objectives of the project are as follows.

- Developing an upper-limb mobile exoskeleton with an anthropomorphic charactersimilar to that of human beings.
- Targeting rehabilitation of shoulder and elbow for Brachial-Plexus injury patientsaffected from partial paralysis of upper limb.
- Exoskeleton actuation under dedicated physiotherapy mode.

